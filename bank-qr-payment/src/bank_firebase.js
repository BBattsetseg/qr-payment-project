import firebase from "firebase/app";
import "firebase/firestore";
import "firebase/auth";

var firebaseConfig = {
    apiKey: "AIzaSyDEgqQWwlX3pZ05Akvy0olgJ5I0VGaM1Aw",
    authDomain: "bank-qr-payment.firebaseapp.com",
    projectId: "bank-qr-payment",
    storageBucket: "bank-qr-payment.appspot.com",
    messagingSenderId: "355313220383",
    appId: "1:355313220383:web:a9683b97546e4179b08e2a"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

export const firestore = firebase.firestore();
export const auth = firebase.auth();
export default firebase;