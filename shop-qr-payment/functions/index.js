const functions = require("firebase-functions");

admin.initializeApp();

//Set-ExecutionPolicy -ExecutionPolicy Bypass -Scope CurrentUser

exports.shop_create_invoice = functions.https.onRequest((request, response) => {
    cors(request, response, async () => {
      console.log(request, response);
      const db = admin.firestore();
      functions.logger.log(request.body);
  
      const invoice = await db.collection("shop_invoices").add({
        name: request.body.name,
        description: request.body.description,
        price: request.body.price,
      });
  
      const res = await axios.post(
        "https://us-central1-qr-payment-13a96.cloudfunctions.net/bank_qr_generate",
        {
          id: invoice.id,
        }
      );
  
      const { qr, invoiceId } = res.data;
  
      response.send({
        qr,
        invoiceId,
      });
    });
  });
  
  exports.shop_recieve = functions.https.onRequest(async (request, response) => {
    const db = admin.firestore();
    functions.logger.log(request.body);
    const res = await db.doc(`shop_invoices/${request.body.id}`).set(
      {
        status: request.body.status,
      },
      {
        merge: true,
      }
    );
  
    response.send({
      'invoice-check': 'complete'
    });
  });
