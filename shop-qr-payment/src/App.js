import logo from "./logo.svg";
import firebase from "./firebase";
import "./App.css";
import axios from "axios";
import { useState } from "react";
function App() {
  const [qrImage, setQrImage] = useState();
  const [isPaid, setIsPaid] = useState(false);
  const [isSend, setIsSend] = useState(false);
  const [info, setInfo] = useState({});

  //immulator ajilluulj bolno, bnbn deploy hiih shaardlagagu
  const getInput = (field) => {
    return (e) => {
      setInfo({
        ...info,
        [field]: e.target.value,
      });
    };
  };

  const onClickButton = async () => {
    const response = await axios.post(
      "https://us-central1-qr-payment-13a96.cloudfunctions.net/shop_create_invoice",
      {
        //form-oosoo avna tsaanaa avahgu zuger damjulj bga
        name: info.name,
        description: info.desc,
        price: info.price,
      }
    );

    const { qr, invoiceId } = response.data;
    setQrImage(qr);
    firebase
      .firestore()
      .doc(`shop_invoices/${invoiceId}`)
      .onSnapshot((doc) => {
        console.log("onsnapshot called");
        console.log(doc.data());
        if (doc.data().status === "paid") {
          setIsPaid(true);
        }
      });
    setIsSend(true);
    console.log(qrImage);
  };
  return (
    <div className="App">
      <header className="App-header">
        {isPaid ? "SUCCESSFULLY PAID" : ""}
        {!isSend && (
          <div>
            <p>Бараагаа оруулна уу</p>
            <form className="form-container">
              <div className="flex">
                <label className="required flex-1">Нэр:</label>
                <input
                  type="text"
                  placeholder="барааны нэр"
                  className=" flex-1"
                  onChange={getInput("name")}
                />
              </div>
              <div className="flex">
                <label className="required flex-1">Тайлбар:</label>
                <input
                  type="text"
                  placeholder="барааны тайлбар"
                  className=" flex-1"
                  onChange={getInput("desc")}
                />
              </div>
              <div className="flex">
                <label className="required flex-1">Үнэ:</label>
                <input
                  type="number"
                  placeholder="барааны үнэ"
                  className=" flex-1"
                  onChange={getInput("price")}
                />
              </div>
            </form>
            <button onClick={onClickButton}>Илгээх!</button>
          </div>
        )}
        {isSend && (
          <div>
            <form className="invoice-container">
              <p>Нэхэмжлэх</p>
              <div className="flex">
                <p className="flex-1">Нэр:</p>
                <p className=" flex-1">{info.name}</p>
              </div>
              <div className="flex">
                <p className="flex-1">Тайлбар:</p>
                <p className=" flex-1">{info.desc}</p>
              </div>
              <div className="flex">
                <p className="flex-1">Үнэ:</p>
                <p className=" flex-1">
                  <b>{info.price} төг</b>
                </p>
              </div>
            </form>
            <p>
              {isPaid
                ? "Төлбөр амжилттай төлөгдлөө."
                : "QR кодоо уншуулж төлбөрөө төлнө үү."}
            </p>
            <img
              src={qrImage ? qrImage : logo}
              className="App-logo"
              alt="logo"
            />
          </div>
        )}
      </header>
    </div>
  );
}

export default App;

