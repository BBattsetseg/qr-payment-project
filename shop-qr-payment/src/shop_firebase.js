import firebase from "firebase/app";
import "firebase/firestore";
import "firebase/auth";

var firebaseConfig = {
    apiKey: "AIzaSyBvgSTwYrxpeG4zjCNO8IrP3K8jep2N-WY",
    authDomain: "shop-qr-payment.firebaseapp.com",
    projectId: "shop-qr-payment",
    storageBucket: "shop-qr-payment.appspot.com",
    messagingSenderId: "902741068885",
    appId: "1:902741068885:web:84e135ea3934d3f95e98dc"
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);


export const firestore = firebase.firestore();
export const auth = firebase.auth();
export default firebase;